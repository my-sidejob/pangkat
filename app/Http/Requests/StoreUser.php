<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->user);

        switch($this->method()){
            case 'POST':
            {
                return [
                    'username' => 'required|unique:users,username',
                    'email' => 'required|unique:users,email',
                    'nama_lengkap' => 'required',
                    'password' => 'required',
                    'username' => 'required',
                    'level' => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'username' => 'required|unique:users,username,' . $user->id,
                    'email' => 'required|unique:users,email,' . $user->id,
                    'nama_lengkap' => 'required',
                    'username' => 'required',
                    'level' => 'required',
                ];
            }
        }
    }
}
