<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class YayasanMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->level == 'yayasan'){
            return $next($request);
        } else {
            return redirect('dashboard')->with('warning', 'Tidak dapat mengakses halaman tersebut');
        }
    }
}
