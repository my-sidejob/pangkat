<?php

namespace App\Http\Middleware;

use App\PangkatPegawai;
use Closure;
use Illuminate\Support\Facades\DB;

class CheckNotification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $records = PangkatPegawai::where('tgl_mulai_bertugas', date('Y-m-d'))->where('status', 2)->get();

        foreach($records as $record){
            $check = DB::table('notifikasi')->where('pangkat_pegawai_id', $record->id)->where('jenis_notifikasi', 'mulai bertugas')->first();
            if($check == null){
                sendNotification($record->id, 'mulai bertugas', $record->user_id, 'Hari ini pegawai yang diajukan sudah mulai aktif bertugas.');
            }
        }
        return $next($request);
    }
}
