<?php

namespace App\Http\Controllers;

use App\Pangkat;
use Illuminate\Http\Request;

class PangkatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pangkat'] = Pangkat::orderBy('nama_pangkat', 'asc')->get();
        return view('pangkat.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['pangkat'] = (object) Pangkat::getDefaultValues();
        return view('pangkat.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_pangkat' => 'required',
        ]);

        Pangkat::create($request->all());
        return redirect()->route('pangkat.index')->with('success', 'Berhasil menambah data pangkat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pangkat'] = Pangkat::find($id);
        return view('pangkat.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_pangkat' => 'required',
        ]);

        Pangkat::find($id)->update($request->all());
        return redirect()->route('pangkat.index')->with('success', 'Berhasil mengubah data pangkat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pangkat::find($id)->delete();
        return redirect()->route('pangkat.index')->with('success', 'Berhasil menghapus data pangkat');
    }
}
