<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NotifikasiController extends Controller
{
    public function index()
    {
        $data['notifikasi'] = DB::table('notifikasi')->where('user_id', Auth::user()->id)->orderBy('tgl_notifikasi', 'desc')->get();
        return view('notifikasi.index', $data);
    }

    public function updateNotif()
    {
        DB::table('notifikasi')->where('user_id', Auth::user()->id)->where('status', 0)->update(['status' => 1]);
    }
}
