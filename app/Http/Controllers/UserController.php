<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\Sekolah;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function index()
    {
        return view ('user.index', ['users' => $this->user->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['user'] = (object) $this->user->getDefaultValues();
        $data['sekolah'] = Sekolah::orderBy('nama_sekolah', 'asc')->get();

        return view('user.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
    	$request['password'] = bcrypt($request['password']);

        User::create($request->toArray());
        return redirect()->route('user.index')->with('success', 'Berhasil menambah data user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = (object) $this->user->find($id);
        $data['sekolah'] = Sekolah::orderBy('nama_sekolah', 'asc')->get();
        return view('user.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUser $request, $id)
    {
        if($request['password'] == ''){
            unset($request['password']);
        } else {
            $request['password'] = bcrypt($request['password']);
        }
        $this->user->find($id)->update($request->all());
        return redirect()->route('user.index')->with('success', 'Berhasil mengubah data user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user->find($id)->delete();
        return redirect()->route('user.index')->with('success', 'Berhasil menghapus data user');
    }
}
