<?php

namespace App\Http\Controllers;

use App\Pangkat;
use App\PangkatPegawai;
use App\Pegawai;
use App\Sekolah;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PangkatPegawaiController extends Controller
{
    public function form()
    {
        if(Auth::user()->level != 'tu'){
            return redirect('dashboard');
        }
        $daftar_pengajuan = PangkatPegawai::where('status', 1)->get()->pluck('pegawai_id')->all();

        $data['pangkat_pegawai'] = PangkatPegawai::getDefaultValues();
        $data['sekolah'] = Sekolah::orderBy('nama_sekolah', 'asc')->get();
        $data['pegawai'] = Pegawai::whereNotIn('id', $daftar_pengajuan)->orderBy('nama_lengkap', 'asc')->get();
        $data['pangkat'] = Pangkat::orderBy('nama_pangkat', 'asc')->get();
        return view('pangkat_pegawai.form', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'pegawai_id' => 'required',
            'sekolah_id' => 'required',
            'pangkat_id' => 'required',
            'jumlah_gaji' => 'required',
            'tgl_diajukan' => 'required',
        ]);

        $input = $request->toArray();
        $input['status'] = 1;
        $input['user_id'] = Auth::user()->id;
        PangkatPegawai::create($input);
        return redirect('daftar-pengajuan')->with('success', 'Pengajuan kenaikan pangkat dan gaji telah terkirim');
    }

    public function daftarPengajuan()
    {
        $user = Auth::user();
        if($user->level == 'tu'){
            $data['pengajuan'] = PangkatPegawai::where('sekolah_id', $user->sekolah_id)->orderBy('tgl_diajukan', 'desc')->get();
        } else {
            $data['pengajuan'] = PangkatPegawai::orderBy('tgl_diajukan', 'desc')->get();
        }

        return view('pangkat_pegawai.index', $data);

    }

    public function detailPengajuan($id)
    {
        $data['pengajuan'] = PangkatPegawai::find($id);
        $data['pangkat_sebelumnya'] = PangkatPegawai::where('pegawai_id', $data['pengajuan']->pegawai_id)->where('tgl_mulai_bertugas', '<', $data['pengajuan']->tgl_diajukan)->orderBy('tgl_mulai_bertugas', 'desc')->first();


        return view('pangkat_pegawai.show', $data);
    }

    public function update(Request $request, $id)
    {
        $input = $request->toArray();
        $input['yayasan_id'] = Auth::user()->id;
        $record = PangkatPegawai::find($id);
        PangkatPegawai::where('id', '!=', $id)->where('pegawai_id', $record->pegawai_id)->update(['status' => 4]);
        $record->update($input);
        if($input['status'] == 2){
            sendNotification($id, 'acc', $record->user_id, 'Pengajuan anda telah diacc oleh yayasan.');
            return redirect('daftar-pengajuan')->with('success', 'Berhasil meng-acc pengajuan');
        }

        if($input['status'] == 3){
            sendNotification($id, 'tolak', $record->user_id, 'Pengajuan anda telah ditolak oleh yayasan.');
            return redirect('daftar-pengajuan')->with('error', 'Berhasil menolak pengajuan');
        }
    }

    public function print($id)
    {
        $data['pengajuan'] = PangkatPegawai::where('id', $id)->where('status', 2)->first();
        $data['pangkat_sebelumnya'] = PangkatPegawai::where('pegawai_id', $data['pengajuan']->pegawai_id)->where('tgl_mulai_bertugas', '<', $data['pengajuan']->tgl_diajukan)->orderBy('tgl_mulai_bertugas', 'desc')->first();
        $pdf = app('dompdf.wrapper');
        $pdf->loadView('pangkat_pegawai.pdf', $data);
        return $pdf->stream();
    }
}
