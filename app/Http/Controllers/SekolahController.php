<?php

namespace App\Http\Controllers;

use App\Sekolah;
use Illuminate\Http\Request;

class SekolahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sekolah'] = Sekolah::orderBy('nama_sekolah', 'asc')->get();
        return view('sekolah.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['sekolah'] = (object) Sekolah::getDefaultValues();
        return view('sekolah.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_sekolah' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
            'nama_kepala_sekolah' => 'required',
            'tahun_didirikan' => 'required',
        ]);

        Sekolah::create($request->all());
        return redirect()->route('sekolah.index')->with('success', 'Berhasil menambah data sekolah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['sekolah'] = Sekolah::find($id);
        return view('sekolah.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_sekolah' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
            'nama_kepala_sekolah' => 'required',
            'tahun_didirikan' => 'required',
        ]);

        Sekolah::find($id)->update($request->all());
        return redirect()->route('sekolah.index')->with('success', 'Berhasil mengubah data sekolah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sekolah::find($id)->delete();
        return redirect()->route('sekolah.index')->with('success', 'Berhasil menghapus data sekolah');
    }
}
