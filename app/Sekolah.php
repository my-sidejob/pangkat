<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    protected $table = 'sekolah';

    protected $fillable = [
        'nama_sekolah',
        'alamat',
        'no_telp',
        'nama_kepala_sekolah',
        'tahun_didirikan',
    ];

    public function pangkatPegawai()
    {
        return $this->hasMany('App\PangkatPegawai');
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public static function getDefaultValues()
    {
        return [
            'nama_sekolah' => '',
            'alamat' => '',
            'no_telp' => '',
            'nama_kepala_sekolah' => '',
            'tahun_didirikan' => '',
        ];
    }

}
