<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PangkatPegawai extends Model
{
    protected $table = 'pangkat_pegawai';

    protected $fillable = [
        'pegawai_id',
        'sekolah_id',
        'pangkat_id',
        'jumlah_gaji',
        'tgl_diajukan',
        'keterangan',
        'status',
        'tgl_mulai_bertugas',
        'user_id',
        'yayasan_id',
        'no_sk',
    ];

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai');
    }
    public function yayasan()
    {
        return $this->belongsTo('App\User');
    }

    public function sekolah()
    {
        return $this->belongsTo('App\Sekolah');
    }

    public function pangkat()
    {
        return $this->belongsTo('App\Pangkat');
    }

    public static function getDefaultValues()
    {
        return (object) [
            'pegawai_id' => '',
            'sekolah_id' => '',
            'pangkat_id' => '',
            'jumlah_gaji' => '',
            'tgl_diajukan' => '',
            'keterangan' => '',
            'status' => '',
            'tgl_mulai_bertugas' => '',
            'user_id' => '',
            'yayasan_id' => '',
            'no_sk' => '',
        ];
    }

    public function getStatus()
    {
        if($this->status == 1){
            return '<span class="badge badge-warning">Diproses</span>';
        }

        if($this->status == 2){
            return '<span class="badge badge-success">Acc</span>';
        }

        if($this->status == 3){
            return '<span class="badge badge-danger">Ditolak</span>';
        }

        if($this->status == 4){
            return '<span class="badge badge-secondary">Tidak aktif</span>';
        }

    }
}
