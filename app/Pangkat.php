<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pangkat extends Model
{
    protected $table = 'pangkat';

    protected $fillable = [
        'nama_pangkat',
        'keterangan',
    ];

    public function pangkatPegawai()
    {
        return $this->hasMany('App\PangkatPegawai');
    }

    public static function getDefaultValues()
    {
        return [
            'nama_pangkat' => '',
            'keterangan' => '',
        ];
    }
}
