<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_lengkap', 'email', 'password', 'username', 'level', 'sekolah_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDefaultValues()
    {
        return [
            'nama_lengkap' => '',
            'email' => '',
            'username' => '',
            'password' => '',
            'level' => '',
            'sekolah_id' => '',
        ];
    }

    public function sekolah()
    {
        return $this->belongsTo('App\Sekolah', 'sekolah_id', 'id');
    }

    public function getLevel()
    {
        if($this->level == 'tu'){
            return 'TU';
        } else {
            return 'Yayasan';
        }
    }


}
