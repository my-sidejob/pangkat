<?php

use Illuminate\Support\Facades\DB;

function urlHasPrefix(string $prefix) {
    $url = url()->current();
    $url_exp = explode('/', $url);

    if ($url_exp[5] == $prefix) {
        return true;
    }

    return false;
}


function sendNotification($pengajuan_id, $jenis_notifikasi, $user_id, $text)
{
    DB::table('notifikasi')->insert([
        'pangkat_pegawai_id' => $pengajuan_id,
        'jenis_notifikasi' => $jenis_notifikasi,
        'tgl_notifikasi' => date('Y-m-d'),
        'keterangan' => $text,
        'user_id' => $user_id,
        'status' => 0,
    ]);
}

function getUnreadNotification()
{
    $data = DB::table('notifikasi')->where('user_id', Auth::user()->id)->where('status', 0)->count();
    if ($data != 0) {
        return '<span class="badge badge-warning">'.$data.'</span>';
    }
}



?>
