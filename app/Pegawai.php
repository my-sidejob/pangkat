<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';

    protected $fillable = [
        'nbm',
        'nama_lengkap',
        'tempat_lahir',
        'tgl_lahir',
        'alamat',
        'no_telp',
        'golongan',
        'jenis_pegawai', //guru, karyawan
        'j_kel',
    ];

    public function pangkatPegawai()
    {
        return $this->hasMany('App\PangkatPegawai');
    }


    public static function getDefaultValues()
    {
        return [
            'nbm' => '',
            'nama_lengkap' => '',
            'tempat_lahir' => '',
            'tgl_lahir' => '',
            'alamat' => '',
            'no_telp' => '',
            'golongan' => '',
            'jenis_pegawai' => '',
            'j_kel' => '',
        ];
    }

    public function getJKel()
    {
        if($this->j_kel == 'l'){
            return 'Laki - Laki';
        } else {
            return 'Perempuan';
        }
    }

    public function getJenisPegawai()
    {
        if($this->jenis_pegawai == 'guru'){
            return '<span class="badge badge-primary">Guru</span>';
        } else {
            return '<span class="badge badge-success">Karyawan</span>';
        }
    }
}
