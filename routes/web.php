<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::middleware(['auth:web', 'check-notification'])->group(function(){
    Route::get('dashboard', 'DashboardController@index');
    Route::resource('sekolah', 'SekolahController');
    Route::resource('pangkat', 'PangkatController');
    Route::resource('pegawai', 'PegawaiController');
    Route::resource('user', 'UserController')->middleware('yayasan-middleware');

    Route::get('form-pengajuan', 'PangkatPegawaiController@form');
    Route::post('form-pengajuan', 'PangkatPegawaiController@store')->name('pengajuan.store');
    Route::get('daftar-pengajuan', 'PangkatPegawaiController@daftarPengajuan');
    Route::get('detail-pengajuan/{id}', 'PangkatPegawaiController@detailPengajuan')->name('pengajuan.detail');
    Route::put('daftar-pengajuan/{id}', 'PangkatPegawaiController@update')->name('pengajuan.update');
    Route::get('print-sk/{id}', 'PangkatPegawaiController@print')->name('pengajuan.print');

    Route::get('notifikasi', 'NotifikasiController@index');
    Route::post('update-notif', 'NotifikasiController@updateNotif');
});
