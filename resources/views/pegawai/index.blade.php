@extends('layouts.app')

@section('title', 'Pegawai')



@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Pegawai</h4>
            </div>
            <div class="card-body">
            @if(Auth::user()->level != 'tu')
            <a href="{{ route('pegawai.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Pegawai</a>
            @endif
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>NBM</th>
                            <th>TTL</th>
                            <th>J. Kel</th>
                            <th>Alamat</th>
                            <th>No. Telp</th>
                            <th>Golongan</th>
                            <th>Jenis Pegawai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($pegawai as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama_lengkap }}</td>
                                <td>{{ $row->nbm }}</td>
                                <td>{{ $row->tempat_lahir . ', ' . $row->tgl_lahir }}</td>
                                <td>{{ $row->getJKel() }}</td>
                                <td>{{ $row->alamat }}</td>
                                <td>{{ $row->no_telp }}</td>
                                <td>{{ $row->golongan }}</td>
                                <td>{!! $row->getJenisPegawai() !!}</td>
                                <td>
                                    <form action="{{ route('pegawai.destroy', $row->id) }}" method="post">
                                        <ul class="d-flex action-button">
                                            <li><a href="{{ route('pegawai.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                            @csrf
                                            @method('delete')
                                            @if(Auth::user()->level != 'tu')
                                            <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li>
                                            @endif
                                        </ul>
                                    </form>
                                </td>
                            </tr>

                        @empty
                            <tr>
                                <td colspan="7">Belum ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
