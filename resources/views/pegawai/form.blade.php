@extends('layouts.app')

@section('title', 'Form Pegawai')



@section('content')
<div class="row ">
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form Pegawai</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($pegawai->id)) ? route('pegawai.store') : route('pegawai.update', $pegawai->id) }}" method="post">
                    @csrf
                    @isset($pegawai->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" placeholder="Nama Pegawai" class="form-control" name="nama_lengkap" value="{{ old('nama_lengkap', $pegawai->nama_lengkap) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>NBM</label>
                                <input type="text" placeholder="NBM" class="form-control" name="nbm" value="{{ old('nbm', $pegawai->nbm) }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" placeholder="Tempat Lahir" class="form-control" name="tempat_lahir" value="{{ old('tempat_lahir', $pegawai->tempat_lahir) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input type="text" placeholder="Tanggal Lahir" class="form-control datepicker2" name="tgl_lahir" value="{{ old('tgl_lahir', $pegawai->tgl_lahir) }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <select name="j_kel" class="form-control mySelect">
                            <option value=""> - Pilih Jenis Kelamin - </option>
                            <option value="l" {{ (old('j_kel') == 'l' || $pegawai->j_kel == 'l') ? 'selected' : '' }}> Laki - Laki</option>
                            <option value="p" {{ (old('j_kel') == 'p' || $pegawai->j_kel == 'p') ? 'selected' : '' }}> Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control" placeholder="Alamat">{{ old('alamat', $pegawai->alamat) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>No. Telp</label>
                        <input type="text" placeholder="No. Telp" class="form-control" name="no_telp" value="{{ old('no_telp', $pegawai->no_telp) }}">
                    </div>
                    <div class="form-group">
                        <label>Golongan</label>
                        <input type="text" placeholder="Golongan" class="form-control" name="golongan" value="{{ old('golongan', $pegawai->golongan) }}">
                    </div>
                    <div class="form-group">
                        <label>Jenis Pegawai</label>
                        <select name="jenis_pegawai" id="" class="form-control">
                            <option value="">- Pilih Jenis Pegawai -</option>
                            <option value="guru" {{ (old('jenis_pegawai') == 'guru' || $pegawai->jenis_pegawai == 'guru') ? 'selected' : '' }}>Guru</option>
                            <option value="karyawan" {{ (old('jenis_pegawai') == 'karyawan' || $pegawai->jenis_pegawai == 'karyawan') ? 'selected' : '' }}>Karyawan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection
