<header class="header my-header">
    <nav class="navbar bg-primary">
        <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
                <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="/" class="navbar-brand">
                    <div class="brand-text d-none d-md-inline-block"><span> Sistem Kenaikan Pangkat dan Gaji</span></div></a>
                </div>
                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                    <!-- Log out-->
                    <form action="{{ (Auth::guard('parent')->check()) ? route('parent.logout') : route('logout') }}" method="post">
                        @csrf
                        <li class="nav-item"><button type="submit" class="nav-link logout btn text-light"> <span class="d-none d-sm-inline-block"><i class="fa fa-sign-out-alt"></i> Logout</span></button></li>
                    </form>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- #33b35a #1f9351 -->
