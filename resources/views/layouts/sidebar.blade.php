<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                <img src="{{ asset('img/icon.jpg') }}" alt="person" class="img-fluid rounded-circle">
                <h2 class="h5">{{ Auth::user()->getLevel() }}</h2>
                <p class="mb-0">{{ Auth::user()->sekolah->nama_sekolah ?? '' }}</p>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>S</strong><strong class="text-primary">I</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->

        <div class="main-menu">
            <h5 class="sidenav-heading">Main</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li class="{{ (urlHasPrefix('dashboard') == true ) ? 'active' : '' }}"><a href="{{ url('dashboard') }}"> <i class="fa fa-palette"></i>Dashboard</a></li>
                <li class="{{ (urlHasPrefix('pegawai') == true ) ? 'active' : '' }}"><a href="{{ url('pegawai') }}"> <i class="fa fa-users"></i>Pegawai</a></li>
                <li class="{{ (urlHasPrefix('sekolah') == true ) ? 'active' : '' }}"><a href="{{ url('sekolah') }}"> <i class="fa fa-home"></i>Sekolah</a></li>
                <li class="{{ (urlHasPrefix('pangkat') == true ) ? 'active' : '' }}"><a href="{{ url('pangkat') }}"> <i class="fa fa-chart-line"></i>Pangkat</a></li>
                @if(Auth::user()->level != 'tu')
                <li class="{{ (urlHasPrefix('user') == true ) ? 'active' : '' }}"><a href="{{ url('user') }}"> <i class="fa fa-user"></i>User</a></li>
                @endif
            </ul>

            <h5 class="sidenav-heading">Kepegawaian</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">
                @if(Auth::user()->level == 'tu')
                <li class="{{ (urlHasPrefix('form-pengajuan') == true ) ? 'active' : '' }}"><a href="{{ url('form-pengajuan') }}"> <i class="fa fa-clipboard-list"></i>Form Pengajuan</a></li>
                @endif
                <li class="{{ (urlHasPrefix('daftar-pengajuan') == true ||  urlHasPrefix('detail-pengajuan') == true) ? 'active' : '' }}"><a href="{{ url('daftar-pengajuan') }}"> <i class="fa fa-clipboard-check"></i>Daftar Pengajuan</a></li>
                @if(Auth::user()->level == 'tu')
                <li class="{{ (urlHasPrefix('notifikasi') == true ) ? 'active' : '' }}"><a href="{{ url('notifikasi') }}"> <i class="fa fa-bell"></i>Notifikasi {!! getUnreadNotification() !!}</a></li>
                @endif
            </ul>

        </div>


    </div>
</nav>
