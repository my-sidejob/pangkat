@extends('layouts.app')

@section('title', 'Daftar Pengajuan Kenaikan Pangkat dan Gaji')



@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Daftar Pengajuan Kenaikan Pangkat dan Gaji</h4>
            </div>
            <div class="card-body">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="pills-diproses-tab" data-toggle="pill" href="#pills-diproses" role="tab" aria-controls="pills-diproses" aria-selected="true">Proses</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="pills-diacc-tab" data-toggle="pill" href="#pills-diacc" role="tab" aria-controls="pills-diacc" aria-selected="false">Acc</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="pills-ditolak-tab" data-toggle="pill" href="#pills-ditolak" role="tab" aria-controls="pills-ditolak" aria-selected="false">Tolak</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-tidak-aktif-tab" data-toggle="pill" href="#pills-tidak-aktif" role="tab" aria-controls="pills-tidak-aktif" aria-selected="false">Tidak Aktif</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-diproses" role="tabpanel" aria-labelledby="pills-diproses-tab">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Pegawai</th>
                                    <th>Sekolah</th>
                                    <th>Pangkat Diajukan</th>
                                    <th>Gaji Diajukan</th>
                                    <th>Tgl. Diajukan</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($pengajuan as $row)
                                    @if($row->status != 1)
                                        @continue
                                    @endif
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->pegawai->nbm . ' - ' . $row->pegawai->nama_lengkap }}</td>
                                        <td>{{ $row->sekolah->nama_sekolah }}</td>
                                        <td>{{ $row->pangkat->nama_pangkat }}</td>
                                        <td>{{ $row->jumlah_gaji }}</td>
                                        <td>{{ $row->tgl_diajukan }}</td>
                                        <td>{!! $row->getStatus() !!}</td>
                                        <td>
                                            <ul class="d-flex action-button">
                                                <li><a href="{{ route('pengajuan.detail', $row->id) }}" class="text-primary" title="Lihat Detail"><i class="fa fa-eye"></i></a></li>
                                                @if(Auth::user()->level == 'tu')
                                                <li><a href="#" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                                @endif
                                            </ul>
                                        </td>
                                    </tr>

                                @empty
                                    <tr>
                                        <td colspan="7">Belum ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="pills-diacc" role="tabpanel" aria-labelledby="pills-diacc-tab">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Pegawai</th>
                                    <th>Sekolah</th>
                                    <th>Pangkat Diajukan</th>
                                    <th>Gaji Diajukan</th>
                                    <th>Tgl. Diajukan</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($pengajuan as $row)
                                    @if($row->status != 2)
                                        @continue
                                    @endif
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->pegawai->nbm . ' - ' . $row->pegawai->nama_lengkap }}</td>
                                        <td>{{ $row->sekolah->nama_sekolah }}</td>
                                        <td>{{ $row->pangkat->nama_pangkat }}</td>
                                        <td>{{ $row->jumlah_gaji }}</td>
                                        <td>{{ $row->tgl_diajukan }}</td>
                                        <td>{!! $row->getStatus() !!}</td>
                                        <td>
                                            <ul class="d-flex action-button">
                                                <li><a href="{{ route('pengajuan.detail', $row->id) }}" class="text-primary" title="Lihat Detail"><i class="fa fa-eye"></i></a></li>

                                            </ul>
                                        </td>
                                    </tr>

                                @empty
                                    <tr>
                                        <td colspan="7">Belum ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="pills-ditolak" role="tabpanel" aria-labelledby="pills-ditolak-tab">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Pegawai</th>
                                    <th>Sekolah</th>
                                    <th>Pangkat Diajukan</th>
                                    <th>Gaji Diajukan</th>
                                    <th>Tgl. Diajukan</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($pengajuan as $row)
                                    @if($row->status != 3)
                                        @continue
                                    @endif
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->pegawai->nbm . ' - ' . $row->pegawai->nama_lengkap }}</td>
                                        <td>{{ $row->sekolah->nama_sekolah }}</td>
                                        <td>{{ $row->pangkat->nama_pangkat }}</td>
                                        <td>{{ $row->jumlah_gaji }}</td>
                                        <td>{{ $row->tgl_diajukan }}</td>
                                        <td>{!! $row->getStatus() !!}</td>
                                        <td>
                                            <ul class="d-flex action-button">
                                                <li><a href="{{ route('pengajuan.detail', $row->id) }}" class="text-primary" title="Lihat Detail"><i class="fa fa-eye"></i></a></li>

                                            </ul>
                                        </td>
                                    </tr>

                                @empty
                                    <tr>
                                        <td colspan="8">Belum ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="pills-tidak-aktif" role="tabpanel" aria-labelledby="pills-tidak-aktif-tab">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Pegawai</th>
                                    <th>Sekolah</th>
                                    <th>Pangkat Diajukan</th>
                                    <th>Gaji Diajukan</th>
                                    <th>Tgl. Diajukan</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($pengajuan as $row)
                                    @if($row->status != 4)
                                        @continue
                                    @endif
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->pegawai->nbm . ' - ' . $row->pegawai->nama_lengkap }}</td>
                                        <td>{{ $row->sekolah->nama_sekolah }}</td>
                                        <td>{{ $row->pangkat->nama_pangkat }}</td>
                                        <td>{{ $row->jumlah_gaji }}</td>
                                        <td>{{ $row->tgl_diajukan }}</td>
                                        <td>{!! $row->getStatus() !!}</td>
                                        <td>
                                            <ul class="d-flex action-button">
                                                <li><a href="{{ route('pengajuan.detail', $row->id) }}" class="text-primary" title="Lihat Detail"><i class="fa fa-eye"></i></a></li>

                                            </ul>
                                        </td>
                                    </tr>

                                @empty
                                    <tr>
                                        <td colspan="8">Belum ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



@endsection
