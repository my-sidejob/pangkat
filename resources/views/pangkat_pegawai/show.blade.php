@extends('layouts.app')

@section('title', 'Detail Pengajuan Kenaikan Pangkat dan Gaji')

@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Detail Pengajuan Kenaikan Pangkat dan Gaji</h4>
            </div>
            <div class="card-body">
                @if(Auth::user()->level != 'tu' && $pengajuan->status == 1)
                <div class="mb-4">
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#formModal">Acc Pengajuan</a> <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#tolakModal">Tolak Pengajuan</a>
                </div>
                @endif
                @if($pengajuan->status == 2)
                <div class="mb-4">
                    <a href="{{ route('pengajuan.print', $pengajuan->id) }}" target="_blank" class="btn btn-info"><i class="fa fa-print"></i> Print SK</a>
                </div>
                @endif
                <table class="table">
                    <tbody>
                        <tr>
                            <td>Nama Pegawai</td>
                            <td>:</td>
                            <td>{{ $pengajuan->pegawai->nbm . ' - ' . $pengajuan->pegawai->nama_lengkap }}</td>
                        </tr>
                        <tr>
                            <td>Sekolah</td>
                            <td>:</td>
                            <td>{{ $pengajuan->sekolah->nama_sekolah }}</td>
                        </tr>
                        <tr>
                            <td>Pangkat Diajukan</td>
                            <td>:</td>
                            <td>{{ $pengajuan->pangkat->nama_pangkat }}</td>
                        </tr>
                        <tr>
                            <td>Gaji Diajukan</td>
                            <td>:</td>
                            <td>{{ $pengajuan->jumlah_gaji }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Diajukan</td>
                            <td>:</td>
                            <td>{{ $pengajuan->tgl_diajukan }}</td>
                        </tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>{!! $pengajuan->getStatus() !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@if($pangkat_sebelumnya != null)
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4>{{ $pangkat_sebelumnya->status == 2 ? "Pangkat dan gaji yang sedang aktif" : "Pangkat dan gaji sebelumnya" }}</h4>
                <table class="table">
                    <tbody>
                        <tr>
                            <td>Nama Pegawai</td>
                            <td>:</td>
                            <td>{{ $pangkat_sebelumnya->pegawai->nbm . ' - ' . $pangkat_sebelumnya->pegawai->nama_lengkap }}</td>
                        </tr>
                        <tr>
                            <td>Sekolah</td>
                            <td>:</td>
                            <td>{{ $pangkat_sebelumnya->sekolah->nama_sekolah }}</td>
                        </tr>
                        <tr>
                            <td>Pangkat Diajukan</td>
                            <td>:</td>
                            <td>{{ $pangkat_sebelumnya->pangkat->nama_pangkat }}</td>
                        </tr>
                        <tr>
                            <td>Gaji Diajukan</td>
                            <td>:</td>
                            <td>{{ $pangkat_sebelumnya->jumlah_gaji }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Diajukan</td>
                            <td>:</td>
                            <td>{{ $pangkat_sebelumnya->tgl_diajukan }}</td>
                        </tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>{!! $pangkat_sebelumnya->getStatus() !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif

<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="formModalLabel">Mohon melengkapi form dibawah</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('pengajuan.update', $pengajuan->id) }}" method="post">
            @csrf
            {{ method_field('PUT')}}
            <div class="modal-body">
                <input type="hidden" name="status" value="2">
                <div class="form-group">
                    <label for="">No. SK</label>
                    <input type="text" name="no_sk" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Tanggal Mulai Bertugas</label>
                    <input type="text" name="tgl_mulai_bertugas" class="form-control datepicker">
                </div>
                <p>Dengan mengisi form tersebut, anda akan men-generate surat secara otomatis yang dapat di download oleh TU sebagai bukti bahwa pengajuan di acc.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
              <button type="submit" class="btn btn-primary">Simpan & Acc</button>
            </div>
        </form>
      </div>
    </div>
</div>

<div class="modal fade" id="tolakModal" tabindex="-1" role="dialog" aria-labelledby="tolakModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="tolakModalLabel">Apakah anda yakin?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('pengajuan.update', $pengajuan->id) }}" method="post">
            @csrf
            {{ method_field('PUT')}}
            <div class="modal-body">
                <input type="hidden" name="status" value="3">
                <p class="mb-0">Pengajuan kenaikan pangkat dan gaji tersebut akan ditolak.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
              <button type="submit" class="btn btn-primary">Simpan & Tolak</button>
            </div>
        </form>
      </div>
    </div>
  </div>


@endsection
