<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $pengajuan->no_sk }}</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }
        .centered {
            text-align: center;
        }
        h2 {
            font-size: 16px;
        }
        h3 {
            font-size: 14px;
        }
        p, td {
            font-size: 13px;
        }
        .hr-header {
            width:300px;
            margin-top:-1px;
        }
        table td, table td * {
            vertical-align: top;
        }
        table {
            width: 100%;
        }
        .table-inside tr td:first-child{
            width: 150px;
        }
        .table-inside tr td:nth-child(2){
            width: 30px;
        }
    </style>
</head>
<body>
    <div class="centered">
        <div style="margin-bottom:5px">
            <img src="{{ public_path('img/logo.png') }}" alt="img" width="100px">
        </div>
        <h2>
            SURAT KEPUTUSAN<br>
            PIMPINAN DAERAH MUHAMMADIYAH<br>
            KOTA DENPASAR
            <hr class="hr-header">
            NOMOR: {{ strtoupper($pengajuan->no_sk) }}
        </h2>
        <h3>
            Tentang<br>
            KENAIKAN PANGKAT DAN GAJI
        </h3>
        <img src="{{ public_path('img/bismillah.jpg') }}" alt="img" width="200px">
    </div>

    <div class="container">
        <table>
            <tr>
                <td>Menimbang</td>
                <td>:</td>
                <td>
                    Bahwa Pegawai Persyarikatan Muhammadiyah Kota Denpasar yang namanya tersebut dalam Surat Keputusan ini, telah memenuhi syarat dan dipandang cakap untuk mendapat kenaikan pangkat dalam keuangan Persyarikatan memungkinkan.<br><br>
                </td>
            </tr>
            <tr>
                <td>Mengingat</td>
                <td>:</td>
                <td>
                    Surat keputusan PWM Bali nomor: 110/II/0/C/2017 tanggal 08 Dzulqo'dah 1438 H / 01 Agustus 2017 M. tentang pedoman Persyarikatan Muhammadiyah / Aisyiyah di bawah badan koordinasi Keuangan Persyarikatan. <br><br>
                </td>
            </tr>
            <tr>
                <td>Memperhatikan</td>
                <td>:</td>
                <td>
                    1. Hasil test kenaikan pangkat tanggal {{ $pengajuan->tgl_mulai_bertugas }}. <br>
                    2. Keputusan Rapat Pleno Majelis Dikdasmen PDM Kota Denpasar tanggal {{ $pengajuan->tgl_mulai_bertugas }}
                </td>
            </tr>
        </table>
    </div>
    <div class="container">
        <div class="centered">
            <h3>MEMUTUSKAN</h3>
        </div>
        <table>
            <tr>
                <td>Pertama</td>
                <td>:</td>
                <td>
                    Pegawai Persyarikatan Muhammadiyah Kota Denpasar :
                    <table class="table-inside">
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td>{{ $pengajuan->pegawai->nama_lengkap }}</td>
                        </tr>
                        <tr>
                            <td>NBM</td>
                            <td>:</td>
                            <td>{{ $pengajuan->pegawai->nbm }}</td>
                        </tr>
                        <tr>
                            <td>Tempat/tanggal lahir</td>
                            <td>:</td>
                            <td>{{ $pengajuan->pegawai->tempat_lahir . ', ' . $pengajuan->pegawai->tgl_lahir }}</td>
                        </tr>
                        @if($pangkat_sebelumnya != null)
                        <tr>
                            <td>Pangkat lama / TMT</td>
                            <td>:</td>
                            <td>{{ $pangkat_sebelumnya->pangkat->nama_pangkat . ' / ' . $pangkat_sebelumnya->tgl_mulai_bertugas }}</td>
                        </tr>
                        <tr>
                            <td>Gaji Pokok Lama</td>
                            <td>:</td>
                            <td>Rp. {{ $pangkat_sebelumnya->jumlah_gaji }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td>Jabatan / Unit Kerja</td>
                            <td>:</td>
                            <td>{{ ucwords($pengajuan->pegawai->jenis_pegawai) . ' / ' . $pengajuan->sekolah->nama_sekolah }}</td>
                        </tr>
                        <tr>
                            <td>Terhitung mulai tanggal</td>
                            <td>:</td>
                            <td>{{ $pengajuan->tgl_mulai_bertugas }}</td>
                        </tr>
                        <tr>
                            <td>Diangkat dalam pangkat</td>
                            <td>:</td>
                            <td>{{ $pengajuan->pangkat->nama_pangkat }}</td>
                        </tr>
                        <tr>
                            <td>Gaji pokok sebesar</td>
                            <td>:</td>
                            <td>Rp. {{ $pengajuan->jumlah_gaji }}</td>
                        </tr>
                    </table>
                    Ditambah dengan penghasilan lain berdasarkan ketentuan persyarikatan<br><br>

                </td>
            </tr>
            <tr>
                <td>Kedua</td>
                <td>:</td>
                <td>Apabila dikemudian hari terdapat kekeliruan dalam surat keputusan ini,akan diadakan pembetulan sebagaimana mestinya</td>
            </tr>
        </table>
    </div>
    <div class="container">

            <div style="width: 50%; display:inline-block">
            </div>
            <div style="width: 50%; display:inline-block">
                <div class="centered">
                    <h3>Denpasar, {{ $pengajuan->tgl_mulai_bertugas }}</h3>
                    <h3>Yayasan</h3>
                    <br>
                    <br>
                    <br>
                    {{ $pengajuan->yayasan->nama_lengkap }}
                </div>
            </div>
    </div>
</body>
</html>
