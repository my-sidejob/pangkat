@extends('layouts.app')

@section('title', 'Form Pengajuan Kenaikan Pangkat dan Gaji')



@section('content')
<div class="row ">
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form Pengajuan Kenaikan Pangkat dan Gaji</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($pangkat_pegawai->id)) ? route('pengajuan.store') : route('pengajuan.update', $pangkat_pegawai->id) }}" method="post">
                    @csrf
                    @isset($pangkat_pegawai->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama Pegawai</label>
                        <select name="pegawai_id" id="" class="form-control mySelect">
                            <option value="">- Pilih Pegawai -</option>
                            @foreach($pegawai as $select)
                                <option value="{{ $select->id }}" {{ (old('pegawai_id') == $select->id || $pangkat_pegawai->pegawai_id == $select->id) ? 'selected' : '' }}> {{ $select->nbm . ' - ' . $select->nama_lengkap }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Sekolah</label>
                                <input type="text" name="" id="" class="form-control readonly" readonly value="{{ Auth::user()->sekolah->nama_sekolah }}">
                                <input type="hidden" name="sekolah_id" id="" class="form-control" value="{{ Auth::user()->sekolah_id }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Pangkat yang diajukan</label>
                                <select name="pangkat_id" id="" class="form-control mySelect">
                                    <option value="">- Pilih Pangkat -</option>
                                    @foreach($pangkat as $select)
                                        <option value="{{ $select->id }}" {{ (old('pangkat_id') == $select->id || $pangkat_pegawai->pangkat_id == $select->id) ? 'selected' : '' }}> {{ $select->nama_pangkat }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Jumlah Gaji</label>
                                <input type="text" name="jumlah_gaji" id="" class="form-control number-input">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Tanggal Diajukan</label>
                                <input type="text" name="tgl_diajukan" id="" class="form-control datepicker">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Keterangan (opsional)</label>
                        <textarea name="keterangan" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection
