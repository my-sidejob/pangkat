@extends('layouts.app')

@section('title', 'Login')

@section('content')

<div class="page login-page">
    <div class="container">

        <div class="form-outer text-center d-flex align-items-center">
            <div class="form-inner w-100">
                <img src="{{ asset('img/logo.png') }}" width="100">
                <br><br>
                <div class="logo text-uppercase"><span></span><strong class="text-primary">LOGIN</strong></div>
                <p>Sistem Kenaikan Pangkat dan Gaji</p>
                @if($errors->any())
                    <hr>
                    @foreach($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                    @endforeach

                @endif
                <form method="post" class="text-left" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group-material">
                        <input id="login-username" type="text" name="username" required data-msg="Please enter your username" class="input-material">
                        <label for="login-username" class="label-material">Username</label>
                    </div>
                    <div class="form-group-material">
                        <input id="login-password" type="password" name="password" required="" data-msg="Please enter your password" class="input-material">
                        <label for="login-password" class="label-material">Password</label>
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" value="LOGIN" class="btn btn-primary">
                    </div>
                </form>
            </div>
            <div class="copyrights text-center">
            <p>&copy; 2021 </p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
        </div>
    </div>
</div>

@endsection
