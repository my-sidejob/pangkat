@extends('layouts.app')

@section('title', 'Pangkat')



@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Pangkat</h4>
            </div>
            <div class="card-body">
            @if(Auth::user()->level != 'tu')
            <a href="{{ route('pangkat.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Pangkat</a>
            @endif
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pangkat</th>
                            <th>Keterangan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($pangkat as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama_pangkat }}</td>
                                <td>{{ $row->keterangan }}</td>
                                <td>
                                    @if(Auth::user()->level != 'tu')
                                    <form action="{{ route('pangkat.destroy', $row->id) }}" method="post">
                                        <ul class="d-flex action-button">
                                            <li><a href="{{ route('pangkat.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                            @csrf
                                            @method('delete')
                                            <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li>
                                        </ul>
                                    </form>
                                    @endif
                                </td>
                            </tr>

                        @empty

                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
