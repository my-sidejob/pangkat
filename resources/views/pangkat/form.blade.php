@extends('layouts.app')

@section('title', 'Form Pangkat')



@section('content')
<div class="row ">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form Pangkat</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($pangkat->id)) ? route('pangkat.store') : route('pangkat.update', $pangkat->id) }}" method="post">
                    @csrf
                    @isset($pangkat->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama Pangkat</label>
                        <input type="text" placeholder="Nama Pangkat" class="form-control" name="nama_pangkat" value="{{ old('nama_pangkat', $pangkat->nama_pangkat) }}">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="keterangan" class="form-control" placeholder="Masukan keterangan">{{ old('keterangan', $pangkat->keterangan) }}</textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection
