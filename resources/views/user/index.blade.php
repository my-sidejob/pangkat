@extends('layouts.app')

@section('title', 'User')



@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data User</h4>
            </div>
            <div class="card-body">
            <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah User</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama User</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Level</th>
                            <th>Sekolah</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($users as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama_lengkap }}</td>
                                <td>{{ $row->username }}</td>
                                <td>{{ $row->email }}</td>
                                <td>{{ $row->getLevel() }}</td>
                                <td>{{ $row->sekolah->nama_sekolah ?? '' }}</td>
                                <td>
                                    <form action="{{ route('user.destroy', $row->id) }}" method="post">
                                        <ul class="d-flex action-button">
                                            <li><a href="{{ route('user.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                            @csrf
                                            @method('delete')
                                            <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li>
                                        </ul>
                                    </form>
                                </td>
                            </tr>
                        @empty

                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
