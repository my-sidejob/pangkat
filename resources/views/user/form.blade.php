@extends('layouts.app')

@section('title', 'Form User')



@section('content')
<div class="row ">
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form User</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($user->id)) ? route('user.store') : route('user.update', $user->id) }}" method="post">
                    @csrf
                    @isset($user->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input type="text" placeholder="Nama Lengkap" class="form-control" name="nama_lengkap" value="{{ old('nama_lengkap', $user->nama_lengkap) }}">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" placeholder="Username" class="form-control" name="username" value="{{ old('username', $user->username) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" placeholder="Password" class="form-control" name="password" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email', $user->email) }}">
                    </div>
                    <div class="form-group">
                        <label>Level</label>
                        <select name="level" id="" class="form-control">
                            <option value="">- Pilih Level -</option>
                            <option value="tu" {{ (old('level') == 'tu' || $user->level == 'tu') ? 'selected' : '' }}>TU</option>
                            <option value="yayasan" {{ (old('level') == 'yayasan' || $user->level == 'yayasan') ? 'selected' : '' }}>Yayasan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Sekolah</label>
                        <select name="sekolah_id" id="" class="form-control">
                            <option value="">- Pilih Sekolah -</option>
                            @foreach($sekolah as $select)
                                <option value="{{ $select->id }}" {{ (old('sekolah_id') == $select->id || $user->sekolah_id == $select->id) ? 'selected' : '' }}> {{ $select->nama_sekolah }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection
