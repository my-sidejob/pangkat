@extends('layouts.app')

@section('title', 'Form Jenis Imunisasi')



@section('content')
<div class="row ">  
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Tambah Jenis Imunisasi</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($jenis_imunisasi->id)) ? route('jenis_imunisasi.store') : route('jenis_imunisasi.update', $jenis_imunisasi->id) }}" method="post">
                    @csrf
                    @isset($jenis_imunisasi->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">       
                        <label>Nama Imunisasi</label>
                        <input type="text" placeholder="Nama Jenis Imunisasi" class="form-control" name="nama_imunisasi" value="{{ old('nama_imunisasi', $jenis_imunisasi->nama_imunisasi) }}">
                    </div>
                    <div class="form-group">       
                        <label>Keterangan</label>
                        <textarea name="keterangan" class="form-control" placeholder="Masukan Keterangan (opsional)">{{ old('keterangan', $jenis_imunisasi->keterangan) }}</textarea>
                    </div>    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>                  
                </form>    
            </div>
        </div>
    </div>
</div>



@endsection
