@extends('layouts.app')

@section('title', 'Example')



@section('content')
<div class="row ">
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Example</h4>
            </div>
            <div class="card-body">
            <a href="#" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Example</a>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Example</th>
                            <th class="text-center">Keterangan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
