@extends('layouts.app')

@section('title', 'Sekolah')



@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Data Sekolah</h4>
            </div>
            <div class="card-body">
            @if(Auth::user()->level != 'tu')
            <a href="{{ route('sekolah.create') }}" class="btn btn-primary btn-sm mb-4"><i class="fa fa-plus"></i> Tambah Sekolah</a>
            @endif
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Sekolah</th>
                            <th>No. Telp</th>
                            <th>Alamat</th>
                            <th>Nama Kepala Sekolah</th>
                            <th>Tahun Didirikan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($sekolah as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama_sekolah }}</td>
                                <td>{{ $row->no_telp }}</td>
                                <td>{{ $row->alamat }}</td>
                                <td>{{ $row->nama_kepala_sekolah }}</td>
                                <td>{{ $row->tahun_didirikan }}</td>
                                <td>
                                    @if(Auth::user()->level != 'tu')
                                    <form action="{{ route('sekolah.destroy', $row->id) }}" method="post">
                                        <ul class="d-flex action-button">
                                            <li><a href="{{ route('sekolah.edit', $row->id) }}" class="text-secondary" title="Edit"><i class="fa fa-edit"></i></a></li>
                                            @csrf
                                            @method('delete')
                                            <li><button type="submit" class="text-danger btn-submit" onclick="return confirm('Yakin akan hapus data?')" ><i class="fa fa-trash"></i></button></li>
                                        </ul>
                                    </form>
                                    @endif
                                </td>
                            </tr>

                        @empty

                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
