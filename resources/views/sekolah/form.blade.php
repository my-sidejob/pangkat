@extends('layouts.app')

@section('title', 'Form Sekolah')



@section('content')
<div class="row ">
    <div class="col-lg-10">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Form Sekolah</h4>
            </div>
            <div class="card-body">
                <form action="{{ (!isset($sekolah->id)) ? route('sekolah.store') : route('sekolah.update', $sekolah->id) }}" method="post">
                    @csrf
                    @isset($sekolah->id)
                        {{ method_field('PUT')}}
                    @endisset
                    <div class="form-group">
                        <label>Nama Sekolah</label>
                        <input type="text" placeholder="Nama Sekolah" class="form-control" name="nama_sekolah" value="{{ old('nama_sekolah', $sekolah->nama_sekolah) }}">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>No. Telp</label>
                                <input type="number" placeholder="No. Telp" class="form-control" name="no_telp" value="{{ old('no_telp', $sekolah->no_telp) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tahun Didirikan</label>
                                <input type="number" placeholder="Tahun Didirikan" class="form-control" name="tahun_didirikan" value="{{ old('tahun_didirikan', $sekolah->tahun_didirikan) }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control" placeholder="Masukan alamat">{{ old('alamat', $sekolah->alamat) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Nama Kepala Sekolah</label>
                        <input type="text" placeholder="Nama Kepala Sekolah" class="form-control" name="nama_kepala_sekolah" value="{{ old('nama_kepala_sekolah', $sekolah->nama_kepala_sekolah) }}">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary float-right" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection
