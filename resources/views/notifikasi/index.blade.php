@extends('layouts.app')

@section('title', 'Notifikasi')



@section('content')
<div class="row ">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h4>Notifikasi</h4>
            </div>
            <div class="card-body">

                <table class="table">
                    <thead>
                        <tr>
                            <th>Pengajuan</th>
                            <th>Jenis Notif</th>
                            <th>Tanggal</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($notifikasi as $row)
                            <tr>
                                <td class="{{ $row->status == 0 ? 'text-bold' : '' }}"><a href="{{ route('pengajuan.detail', $row->pangkat_pegawai_id) }}">Pengajuan-{{ $row->pangkat_pegawai_id }}</a></td>
                                <td class="{{ $row->status == 0 ? 'text-bold' : '' }}">{{ $row->jenis_notifikasi }}</td>
                                <td class="{{ $row->status == 0 ? 'text-bold' : '' }}">{{ $row->tgl_notifikasi }}</td>
                                <td class="{{ $row->status == 0 ? 'text-bold' : '' }}">{{ $row->keterangan }}</td>
                            </tr>
                        @empty

                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection

@push('scripts')

<script>
    $(document).ready(function(){
        $.ajax({
            url: "{{ url('update-notif') }}",
            method: "post",
            success: function(e){

            }
        });
    });
</script>

@endpush
