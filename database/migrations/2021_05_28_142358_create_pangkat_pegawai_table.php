<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePangkatPegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pangkat_pegawai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('pegawai_id');
            $table->unsignedBigInteger('sekolah_id');
            $table->unsignedBigInteger('pangkat_id');
            $table->string('jumlah_gaji');
            $table->date('tgl_diajukan');
            $table->string('keterangan')->nullable();
            $table->string('status');
            $table->date('tgl_mulai_bertugas')->nullable();
            $table->integer('user_id');
            $table->integer('yayasan_id')->nullable();
            $table->foreign('pegawai_id')->references('id')->on('pegawai')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('sekolah_id')->references('id')->on('sekolah')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('pangkat_id')->references('id')->on('pangkat')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pangkat_pegawai');
    }
}
